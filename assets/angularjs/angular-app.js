var app = angular.module("adminApp",["ui.router"]);

app.config(function($stateProvider,$urlRouteProvider,$locationProvider){
	$stateProvider
	.state('admin',{
		url:'/',
		templateUrl: BASE_URL+'admin/admin'
	})
	.state('slider',{
		url:'/slider',
		templateUrl:BASE_URL+'admin/admin/slider'
	});
	$urlRouteProvider.otherwise('/')	
});

