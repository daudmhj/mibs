           </div>
        <!-- /#page-wrapper -->

    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jQuery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/sb-admin-2.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/angularjs/angular.min.js"></script>
    <script src="<?php echo base_url()?>assets/angularjs/angular-route.js"></script>
    <script src="<?php echo base_url()?>assets/angularjs/angular-app.js"></script>

</body>

</html>