<div class="container-fluid">
	<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Slider <small>Panel Admin</small>
                        </h1>
                        <p>Halaman untuk mengganti slider landing page pada halaman website.</p>
                        <p>Ukuran yang disarankan untuk Slider adalah 750px x 300px</p>
                    </div>


<!-- form update slider -->
    <div id="addSlider" class="slider-konten">
    	<div class="col-md-12 slider-add">
    	<h3>Tambah Gambar Slider</h3>
    		<form role="form" method="POST" action="<?php echo base_url();?>admin/Slider/insertSlider" enctype="multipart/form-data">
    			
                <div class="col-xs-4">
                    <input name="id" type="text" class="form-control" disabled value="<?php echo $data['ID'] ?>">
                </div>

                <div class="col-xs-4">
                    <input name="slider" type="text" class="form-control" placeholder="Slider" value="<?php echo $data['Slider'] ?>">
                </div>
                <br>
                <br>
                <p><label> <i class="fa fa-upload" style="margin-right: 5px;"></i>Upload Gambar (750px x 300px)</label></br>
    			<input type="file" name="path" /></p>
    			<p><label>Caption</label></br>
    			<textarea id="slider-caption" class="slider-caption" name="caption"><?php echo $data['Caption'] ?></textarea></p>
    			<p class="btn-group-slider"><input type="submit" value="Tambah" name="submit"></p><a href="<?php echo base_url()?>admin/slider"><i class="fa fa-arrow-left"></i> Kembali</a>
    		</form>
    	</div>
    </div>
</div>
</div>