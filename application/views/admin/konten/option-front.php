<div class="container-fluid">
	<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Pengaturan <small> Halaman Depan</small>
                        </h1>

					</div>
	
				<div class="row col-md-12 custyle">

    <!-- tabel -->
   <div id="tabelSlider" class="slider-konten"> 
    <table class="table table-striped custab">
    <thead>
        <tr>
            <th>ID</th>
            <th>Halaman</th>
            <th>Deskripsi</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <?php foreach ($pages as $page) {?>
            <tr> <!-- di foreach -->
                <td><?php echo $page['id']?></td>
                <td><?php echo $page['nama']?></td>
                <td><?php echo substr($page['isi_hlm'],0,70)?></td>
                <td class="text-center"><a class='btn btn-info btn-xs' href="<?php echo base_url()."admin/pengaturan/editHalaman/".$page['id'];?>"><span class="glyphicon glyphicon-edit"></span> Edit</a></td>
            </tr>
            <?php }?>
    </table>
    </div>
<!-- web info -->
    <div class="web-info" id="web-info-1">
        <h3>Informasi Website</h3>
        <div class="tab-info" style="margin-left: 0">
            <label>Nama Website</label>
            <p><input type="text" name="nama" disabled value="<?php echo $nama['isi_kolom'];?>"></p></br>
        </div>
        <div class="tab-info">
            <label>Tagline</label>
            <p><input type="text" name="tagline" disabled value="<?php echo $tag['isi_kolom'];?>"></p></br>
        </div>
        <div class="tab-info">
            <label>Nomor Telepon</label>
            <p><input type="text" name="telepon" disabled value="<?php echo $tel['isi_kolom'];?>"></p></br>
        </div>
        <div class="tab-info">
            <label>Alamat</label>
            <p><input type="text" name="alamat" disabled value="<?php echo $add['isi_kolom'];?>"></p></br>
        </div>
        <p><label>Sosial Media</label></p>
        <p><div class="web-info-sc">Facebook  :</div> <input type="text" name="telepon" disabled value="<?php echo $fb['isi_kolom'];?>"></p>
        <p><div class="web-info-sc">Twitter   :</div> <input type="text" name="telepon" disabled value="<?php echo $tw['isi_kolom'];?>"></p>
        <p><div class="web-info-sc">Instagram :</div> <input type="text" name="telepon" disabled value="<?php echo $ig['isi_kolom'];?>"></p>
        <p><div class="web-info-sc">Line      :</div> <input type="text" name="telepon" disabled value="<?php echo $line['isi_kolom'];?>"></p>
        <p><input id="web-info-btn" type="button" href="#" name="edit" value="Edit" onclick="editWebinfo(this)"></p>
    </div> <!-- web info-->

    <div class="web-info-edit" id="web-info-2" style="display: none;">
        <form method="POST" action="<?php echo base_url() . "admin/pengaturan/updateInfo"?>">
            <h3>Ubah Informasi Website</h3>
        <div class="tab-info" style="margin-left: 0">
            <label>Nama Website</label>
            <p><input type="text" name="namaWeb" value="<?php echo $nama['isi_kolom'];?>" placeholder="Masukkan Nama Website.."></p></br>
        </div>
        <div class="tab-info">
            <label>Tagline</label>
            <p><input type="text" name="taglineWeb" placeholder="Masukkan Tagline Website.." value="<?php echo $tag['isi_kolom'];?>"></p></br>
        </div>
        <div class="tab-info">
            <label>Nomor Telepon</label>
            <p><input type="text" name="teleponWeb" placeholder="Masukkan No.Telepon.." value="<?php echo $tel['isi_kolom'];?>"></p></br>
        </div>
        <div class="tab-info">
            <label>Alamat</label>
            <p><input type="text" name="alamatWeb" placeholder="Masukkan Alamat.." value="<?php echo $add['isi_kolom'];?>"></p></br>
        </div>
        <p><label>Sosial Media</label></p>
        <p><div class="web-info-sc">Facebook  :</div> <input type="text" name="fbWeb" placeholder="http://" value="<?php echo $fb['isi_kolom'];?>"></p>
        <p><div class="web-info-sc">Twitter   :</div> <input type="text" name="twWeb" placeholder="http://" value="<?php echo $tw['isi_kolom'];?>"></p>
        <p><div class="web-info-sc">Instagram :</div> <input type="text" name="igWeb" placeholder="http://" value="<?php echo $ig['isi_kolom'];?>"></p>
        <p><div class="web-info-sc">Line      :</div> <input type="text" name="lineWeb" placeholder="http://" value="<?php echo $line['isi_kolom'];?>"></p>
        <p><input type="submit" name="edit" value="Simpan"><input type="button" name="edit" value="Batal" style="margin-left: 10px;" href="#" id="web-info-batal" onclick="editWebinfo(this)"></p>
        </form>
    </div>


</div> <!-- div row col-md-8 -->

</div>
</div>
