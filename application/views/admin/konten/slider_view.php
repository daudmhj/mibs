<div class="container-fluid">
	<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Slider <small>Panel Admin</small>
                        </h1>
                        <p>Halaman untuk mengganti slider landing page pada halaman website.</p>
                        <p>Ukuran yang disarankan untuk Slider adalah 750px x 300px</p>
                    </div>

<div class="row col-md-12 custyle">
    <p><a href="javascript:void(0)" id="addSlider-btn" class="btn-slider pull-right" style="float: right;" onclick="changeSlider(event,'addSlider')"><b>+</b> Tambah Slider Baru</a><a href="javascript:void(0)" id="tabelSlider-btn" class="btn-slider pull-right" onclick="changeSlider(event,'tabelSlider')"><i class="fa fa-table"></i> Tabel</a></p>


<!-- form tambah slider -->
    <div id="addSlider" class="slider-konten">
    	<div class="col-md-12 slider-add">
    	<h3>Tambah Gambar Slider</h3>
    		<form role="form" method="POST" action="<?php echo base_url();?>admin/Slider/insertSlider" enctype="multipart/form-data">
    			
                <div class="col-xs-4">
                    <input name="id" type="text" class="form-control" placeholder="ID">
                </div>

                <div class="col-xs-4">
                    <input name="slider" type="text" class="form-control" placeholder="Slider">
                </div>
                <br>
                <br>
                <p><label> <i class="fa fa-upload" style="margin-right: 5px;"></i>Upload Gambar (750px x 300px)</label></br>
    			<input type="file" name="path" /></p>
    			<p><label>Caption</label></br>
    			<textarea id="slider-caption" class="slider-caption" name="caption"></textarea></p>
    			<p class="btn-group-slider"><input type="submit" value="Tambah" name="submit"><input type="reset" value="Reset" name="reset"></p>
    		</form>
    	</div>
    </div>

    <!-- tabel -->
   <div id="tabelSlider" class="slider-konten"> 
    <table class="table table-striped custab">
    <thead>
        <tr>
            <th>ID</th>
            <th>Slider</th>
            <th>Caption</th>
            <th>Gambar</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
            <!-- di foreach -->
                <?php
                foreach ($data as $row) 
                {
                    echo
                    '<tr><td>'.$row['ID'].'</td>
                    <td>'.$row['Slider'].'</td>
                    <td>'.$row['Caption'].'</td>
                    <td><img src="'.base_url().$row['Gambar'].'" width="100px"/></td>
                    <td class="text-center"><a class="btn btn-info btn-xs" href="'.base_url().'admin/slider/sliderUpdate/'.$row['ID'].'" ><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td></tr>';
                }
                ?>
    </tbody>
    </table>
    </div>


</div> <!-- div row col-md-8 -->

                    <!-- div row -->
                </div>
</div>

<script>

</script>