<div class="container-fluid">
	<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Testimoni <small>Panel Admin</small>
                        </h1>
					</div>
<!-- sisi penerbit -->
		<div class="col-md-6 col-sm-6">
			<div class="penerbit-col">	
			<h4>Penerbit</h4>
			<div class="add-penerbit">
				<form method="POST" action="<?php echo base_url('admin/collection/addPenerbit') ?>">
					<p><input type="text" name="nama" placeholder="tambahkan penerbit disini" required=""><input type="submit" value="Tambah"></p><?php echo form_error('nama'); ?>
				</form>
				<?php if (isset($msg_pen)) { ?>
				<CENTER><h5 style="color:green;">Berhasil Ditambahkan</h5></CENTER><br>
				<?php } ?>
			</div>

			<div class="tabel-penerbit">
			<div id="tabelSlider" class="slider-konten"> 
		    <table class="table table-striped custab">
		    <thead>
		        <tr>
		            <th>ID</th>
		            <th>Penerbit</th>
		            <th class="text-center">Action</th>
		        </tr>
		    </thead>

		     <?php foreach ($penerbit as $row){?>
		            <tr> <!-- di foreach -->
		                <td><?php echo $row['id'] ?></td>
		                <td><?php echo $row['nama'] ?></td>
		                <td class="text-center"><a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
		            </tr>
		            <?php } ?>
		    </table>
		    </div>
			</div>		
			</div>
			</div>

<!-- sisi kategori -->
			<div class="col-md-6 col-sm-6">
			<div class="penerbit-col">	
			<h4>Kategori</h4>
			<div class="add-penerbit">
				<form method="POST" action="">
					<p><input type="text" name="" placeholder="tambahkan penerbit disini"><input type="submit" value="Tambah"></p>
				</form>
			</div>

			<div class="tabel-penerbit">
			<div id="tabelSlider" class="slider-konten"> 
		    <table class="table table-striped custab">
		    <thead>
		        <tr>
		            <th>ID</th>
		            <th>Kategori</th>
		            <th class="text-center">Action</th>
		        </tr>
		    </thead>

		    <?php foreach ($categories as $row){?>
		            <tr> <!-- di foreach -->
		                <td><?php echo $row['id'] ?></td>
		                <td><?php echo $row['nama'] ?></td>
		                <td class="text-center"><a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
		            </tr>
		            <?php } ?>
		    </table>
		    </div>
			</div>		
			</div>
			</div>

</div>
</div>					