<div class="container-fluid">
	<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Testimoni <small>Panel Admin</small>
                        </h1>

					</div>
	
		<div class="row col-md-12 custyle">
		    <p><a href="javascript:void(0)" id="addSlider-btn" class="btn-slider pull-right" style="float: right;" onclick="changeSlider(event,'addSlider')"><b>+</b> Tambah Testimoni</a><a href="javascript:void(0)" id="tabelSlider-btn" class="btn-slider pull-right" onclick="changeSlider(event,'tabelSlider')"><i class="fa fa-comment"></i> Testimoni</a></p>


		<!-- form tambah testi -->
		    <div id="addSlider" class="slider-konten">
		    	<div class="col-md-12 testi-add">
		    	<h3> Tambah Testimoni</h3>
		    		<form>
		    			<p><label> <i class="fa fa-users" style="margin-right: 5px;"></i> Nama Pelanggan</label></br>
		    			<input type="text" name="nama-testi" /></p>
		    			<p><label><i class="fa fa-comment"></i> Testimoni</label></br>
		    			<textarea id="slider-caption" class="slider-caption" name="testimoni"></textarea></p>
		    			<p class="btn-group-slider"><input type="submit" value="Tambah" name="submit"><input type="reset" value="Cancel" name="reset"></p>
		    		</form>
		    	</div>
		    </div>

		    <!-- tabel -->
		   <div id="tabelSlider" class="slider-konten"> 
		    <table class="table table-striped custab">
		    <thead>
		        <tr>
		            <th>ID</th>
		            <th>Nama</th>
		            <th>Testimoni</th>
		            <th class="text-center">Action</th>
		        </tr>
		    </thead>
		            <tr> <!-- di foreach -->
		                <td>1</td>
		                <td>Nama pelanggan</td>
		                <td>Testimoni palanggan Testimoni palanggan Testimoni palanggan Testimoni palanggan Testimoni palanggan Testimoni palanggan Testimoni palanggan Testimoni palanggan Testimoni palanggan Testimoni palanggan Testimoni palanggan Testimoni palanggan </td>
		                <td class="text-center"> <a class='btn btn-success btn-xs' href="<?php echo base_url('admin/testimoni/updateTesti') ?>"><i class="fa fa-pencil-square-o"></i> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
		            </tr>
		    </table>
		    </div>


		</div> <!-- div row col-md-8 -->


	</div>
</div>