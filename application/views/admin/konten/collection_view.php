<div class="container-fluid">
	<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Koleksi Buku <small>Panel Admin</small>
                        </h1>
                        <p></p>
                    </div>
                
   <div class="row col-md-12 custyle">
		    <p><a href="javascript:void(0)" id="addSlider-btn" class="btn-slider pull-right" style="float: right;" onclick="changeSlider(event,'addSlider')"><b>+</b> Tambah Koleksi</a><a href="javascript:void(0)" id="tabelSlider-btn" class="btn-slider pull-right" onclick="changeSlider(event,'tabelSlider')"><i class="fa fa-book"></i> Koleksi</a></p>


		<!-- form tambah buku -->
		    <div id="addSlider" class="slider-konten">
		    	<div class="col-md-12 testi-add colect-add">
		    	<h3> Tambah Koleksi</h3>
		    		<form>
		    			<p><label>Judul Buku</label><br>
		    			<input type="text" name="judul-buku" /></p>
		    			<p><label>Pengarang</label></br>
		    			<input type="text" name="testimoni"></p>
		    			<p><label>Penerbit</label></br>
		    			<select required name="penerbit">
		    			<!-- di foreach dari database peberbit -->
		    				<option value="none" selected="selected">--Pilih Penerbit--</option>
		    				<?php foreach($shPenerbit as $pen){ ?>
		    				<option value="<?php echo $pen->id?>"><?php echo $pen->nama?></option>
		    				<?php }?>

		    			</select></p>
		    			<p><label>Kategori</label></br>
		    			<select required>
		    			<!-- di foreach dari database categori -->
		    				<option>--Pilih Kategori--</option>
		    				<option>Lawak</option>
		    			</select></p>
		    			<p><label>ISBN</label></br>
		    			<input type="text" name="testimoni"></p>
		    			<p><label>Harga</label></br>
		    			IDR <input type="number" name="testimoni"></p>
		    			<p><label>Gambar</label></br>
		    			<input type="file" id="slider-caption" class="slider-caption" name="testimoni"></p>
		    			<p><label>Deskripsi</label></br>
		    			<textarea></textarea></p>
		    			<p class="btn-group-slider"><input type="submit" value="Tambah" name="submit"><input type="reset" value="Reset" name="reset"></p>
		    		</form>
		    	</div>
		    </div>

		    <!-- tabel -->
		   <div id="tabelSlider" class="slider-konten"> 
		   <!-- Search -->

		   <form id="custom-search-input" method="post" action="">
                            <div class="input-group col-md-4">
                                <input type="text" name="cari" class="  search-query form-control" placeholder="Cari semua berhubungan dengan buku..." />
                                <span class="input-group-btn">
                                    <button class="btn btn-success" type="submit" value="search" name="q">
                                        <span class=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                        </form>


		    <table class="table table-striped custab">
		    <thead>
		        <tr>
		            <th>ID</th>
		            <th>Judul</th>
		            <th>Pengarang</th>
		            <th>Penerbit</th>
		            <th>Harga</th>
		            <th class="text-center">Action</th>
		        </tr>
		    </thead>
		            <tr> <!-- di foreach -->
		                <td>(Auto increment)</td>
		                <td>Cinta Buta</td>
		                <td>Maxwell</td>
		                <td>Dar! Mizan</td>
		                <td>IDR 50.000</td>
		                <td class="text-center"> <a class='btn btn-success btn-xs' href="<?php echo base_url('admin/collection/bookUpdate') ?>"><i class="fa fa-pencil-square-o"></i> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
		            </tr>
		    </table>
		    </div>


		</div> <!-- div row col-md-8 -->

 </div>

</div>