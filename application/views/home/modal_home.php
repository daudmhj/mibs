 <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 999999">
            <div class="modal-dialog">
              <div class="modal-content modal-content-one">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                  <h6 class="modal-title" id="myModalLabel"><?php echo $cara['nama']; ?></h6>
                </div>
                <div class="modal-body msg-modal">
                  <div class="hlm-view">
                  <img src="<?php echo $cara['gambar_hlm']; ?>">
                  </div>
                  <div class="msg-view-area">
                   <?php echo $cara['isi_hlm']; ?>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>

 <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 999999">
            <div class="modal-dialog">
              <div class="modal-content modal-content-one">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                  <h6 class="modal-title" id="myModalLabel"><?php echo $about['nama']; ?></h6>
                </div>
                <div class="modal-body msg-modal">
                  <div class="hlm-view">
                  <img src="<?php echo $about['gambar_hlm']; ?>">
                  </div>
                  <div class="msg-view-area">
                   <?php echo $about['isi_hlm']; ?>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>

 <div class="modal fade" id="modal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 999999">
            <div class="modal-dialog">
              <div class="modal-content modal-content-one">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                  <h6 class="modal-title" id="myModalLabel"><?php echo $faq['nama']; ?></h6>
                </div>
                <div class="modal-body msg-modal">
                  <div class="hlm-view">
                  <img src="<?php echo $faq['gambar_hlm']; ?>">
                  </div>
                  <div class="msg-view-area">
                   <?php echo $faq['isi_hlm']; ?>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>          