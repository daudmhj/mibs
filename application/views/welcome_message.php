<!DOCTYPE html>
<html lang="en-us">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scaled=1.0">

	<title>MIBS</title>


<!-- =================================CSS====================== -->
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css"/>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css"/>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jQuery.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrapValidator.min.js"></script>
</head>
<body>

<input id="toggle" type="checkbox" />
<nav>
  
  <ul id="nav">
 <!--  <label class="close" for="toggle" onclick="closeNav()">&times;</label> -->
    <li class="current"><a href="">Home</a></li>
    <li><a href="">Catalog</a></li>
    <li><a href="#" data-toggle="modal" data-target="#modal1">Cara Pembelian</a></li>
    <li><a href="#" data-toggle="modal" data-target="#modal2">About</a></li>
    <li><a href="#" data-toggle="modal" data-target="#modal3">FAQ</a></li>
    <li><a href="<?php echo site_url('admin/admin');?>">Login</a></li>
  </ul>
</nav>
 
<div class="wrapper">
  <div class="inner">
    <header class="header">
      <label class="btn" for="toggle" onclick="myToggle(this)">
      	<span>
      		<div class="bar1"></div>
			<div class="bar2"></div>
			<div class="bar3"></div>
      	</span>
      </label>
      <div class="title">
      <img src="<?php echo base_url();?>assets/img/mibs.png">
      </div>
    </header>
<!-- awal dari section 1 -->
<div class="section1">
	<div class="container">
		<div class="col-md-6 col-sm-6">
		<div class="logo-red">
			<img src="<?php echo base_url();?>assets/img/mibs-red.png">
		</div>
		</div>
		<div class="col-md-6 col-sm-6">
		<div class="indentitas-utama">
		<div class="indentitas" style="margin-top: 20px">
			<img src="<?php echo base_url();?>assets/img/tel.png">
			<?php echo $tel['isi_kolom']; ?>
		</div>
		<div class="indentitas" style="margin-bottom: 20px;">
			<img src="<?php echo base_url();?>assets/img/loc.png">
			<?php echo $alamat['isi_kolom']; ?>
		</div>
		<div class="indentitas" style="margin-bottom: 20px;">
			<form>
			<div class="search-form">
			  <input type="text" name="search" placeholder="Cari buku..">
			  <button>CARI</button>
			</div>
			</form>
		</div>
		</div>
		</div>
	</div>
</div>
<!-- awal dari content  1 -->
<div class="content1">
<div class="container"> <!-- batas ukuran samping -->
<!-- slide show -->
		
		<div id="myCarousel" class="carousel slide" data-ride="carousel" style="max-width: 750px">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php
            	$enum =1;
            	foreach($data as $row){
              	echo '<li data-target="#main-slider" data-slide-to="'.$enum.'" class="'; if($enum==1){echo 'active';}; echo '"></li>';
              	$enum+=1;
            	}
          		?>
			</ol>

			<!-- deklarasi carousel -->
			<div class="carousel-inner" role="listbox">
				<?php
          $enum =1;
          foreach($data as $row){
            echo '<div class="item '; if($enum==1){echo 'active';}; echo '"style="max-height: 300px;">

            	<img src="'.base_url().$row['Gambar'].'" alt="MIBS">
					<div class="carousel-caption">
						<h3>'.$row['Caption'].'</h3>
					</div>
            </div>';
            $enum+=1;
          }
        ?>	
			</div>

			

			<!-- membuat panah next dan previous -->
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
</div>
<!-- konten featured book -->
<div class="container">
	<div class="featured-book">
	<h3><?php echo $nama['isi_kolom']; ?></h3>
	<h5><?php echo $tagline['isi_kolom']; ?></h5>
	</div>

	<div class="catalog-text">
	<h4>CATALOG</h4>
	</div>
	<div class="col-md-4 col-sm-4">
	<div class="featured-book-1">
		<div class="featured-book-img">
			<img src="<?php echo base_url();?>assets/img/book1.jpg">
		</div>
		<div class="featured-book-content">
		<div class="featured-book-title">
		<h4>Lorem Ipsum</h4>
		</div>
		<!-- <div class="featured-book-txt">
		<h5>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem</h5>
		</div> -->
		<div class="featured-book-prc">
			<h4>IDR 100.000</h4>
		</div>
		<div class="featured-book-btn">

		</div>
		</div>
	</div>
	</div>
	<div class="col-md-4 col-sm-4">
	<div class="featured-book-1">
		<div class="featured-book-img">
			<img src="<?php echo base_url();?>assets/img/book1.jpg">
		</div>
		<div class="featured-book-content">
		<div class="featured-book-title">
		<h4>Lorem Ipsum</h4>
		</div>
		<!-- <div class="featured-book-txt">
		<h5>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem</h5>
		</div> -->
		<div class="featured-book-prc">
			<h4>IDR 100.000</h4>
		</div>
		<div class="featured-book-btn">

		</div>
		</div>
	</div>
	</div>
	<div class="col-md-4 col-sm-4">
	<div class="featured-book-1">
		<div class="featured-book-img">
			<img src="<?php echo base_url();?>assets/img/book1.jpg">
		</div>
		<div class="featured-book-content">
		<div class="featured-book-title">
		<h4>Lorem Ipsum</h4>
		</div>
		<!-- <div class="featured-book-txt">
		<h5>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem</h5>
		</div> -->
		<div class="featured-book-prc">
			<h4>IDR 100.000</h4>
		</div>
		<div class="featured-book-btn">

		</div>
		</div>
	</div>
	</div>
</div>

		<!-- About && testi  -->
<div class="section-about">
	<div class="container">
	<div class="col-md-6 col-sm-6">
	<div class="section-about-block">
		<h4>About</h4>
		<p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem</p>
	</div>
	</div>
	<div class="col-md-6 col-sm-6">
	<div class="section-about-block">
		<h4>Testimoni</h4>
<!-- 		<p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem</p> -->

	<div id="text-carousel" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="row">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="carousel-content">
                        <div>
                            <p>Sapiente, ducimus, voluptas, mollitia voluptatibus nemo explicabo sit blanditiis laborum dolore illum fuga veniam quae expedita libero accusamus quas harum ex numquam necessitatibus provident deleniti tenetur iusto officiis recusandae corporis culpa quaerat?</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="carousel-content">
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, sint fuga temporibus nam saepe delectus expedita vitae magnam necessitatibus dolores tempore consequatur dicta cumque repellendus eligendi ducimus placeat! </p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="carousel-content">
                        <div>                          
                            <p>Sapiente, ducimus, voluptas, mollitia voluptatibus nemo explicabo sit blanditiis laborum dolore illum fuga veniam quae expedita libero accusamus quas harum ex numquam necessitatibus provident deleniti tenetur iusto officiis recusandae corporis culpa quaerat?</p>
                        </div>
                    </div>
                </div>
                
            </div>
    </div>
    <!-- Indicators -->
			<ol class="carousel-indicators" >
				<li data-target="#text-carousel" data-slide-to="0" class="active" ></li>
				<li data-target="#text-carousel" data-slide-to="1"></li>
				<li data-target="#text-carousel" data-slide-to="2"></li>		
			</ol>
</div>

	</div>
	</div>
	</div>
</div>
 
 <!-- footer -->
<div class="footer-1">
<div class="container">
	<img src="<?php echo base_url();?>assets/img/fb.png">
	<img src="<?php echo base_url();?>assets/img/ig.png">
	<img src="<?php echo base_url();?>assets/img/twitter.png">
	<img src="<?php echo base_url();?>assets/img/line.png">
	<h4>&copy; MIBS 2017</h4>
</div>
</div>

 <!-- footer 2 -->
 <div class="footer-2">
 	<div class="container">
 		<div class="col-md-6 col-sm-6" style="margin-bottom: 20px;">
 		<h5>Kritik dan saran</h5>
 			<form role="form" id="contact-form" class="contact-form">
                    <div class="row">
                		<div class="col-md-6">
                  		<div class="form-group">
                            <input type="text" class="form-control" name="Name" autocomplete="off" id="Name" placeholder="Name">
                  		</div>
                  	</div>
                    	<div class="col-md-6">
                  		<div class="form-group">
                            <input type="email" class="form-control" name="email" autocomplete="off" id="email" placeholder="E-mail">
                  		</div>
                  	</div>
                  	</div>
                  	<div class="row">
                  		<div class="col-md-12">
                  		<div class="form-group">
                            <textarea class="form-control textarea" rows="3" name="Message" id="Message" placeholder="Message"></textarea>
                  		</div>
                  	</div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                  <button type="submit" class="btn main-btn pull-right">Kirim Pesan</button>
                  </div>
                  </div>
                </form>
 		</div>
 		
 		<div class="col-md-6 col-sm-6">
 		<div class="maps">
 		<h5>Lokasi</h5>
 			<div id="map" style="width:80%;height:300px;
  margin-left:10%; margin-bottom: 20px;"></div>
 			<script>
			function myMap() {
			  var mapCanvas = document.getElementById("map");
			  var mapOptions = {
			    center: new google.maps.LatLng(-7.282324, 112.793054),
			    zoom: 18
			  }
			  var map = new google.maps.Map(mapCanvas, mapOptions);
			}
			</script>
			<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
 		</div>
 		</div>
 	</div>
 </div>

</body>
</html>