<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
        $this->load->model('Model_slider');
        $this->load->model('Model_pengaturan');
	}

	public function index()
	{
		// $this->session->sess_destroy();
		$table = "slider";
		$table2 = 'pages';
		$table3 = 'options';
        $data1 = array( 'cara' =>  $this->Model_pengaturan->get_info_id('2',$table2),
        				'about' =>  $this->Model_pengaturan->get_info_id('1',$table2),
        				'faq' => $this->Model_pengaturan->get_info_id('3',$table2));

        $data =array( 'nama' => $this->Model_pengaturan->get_info_id('1',$table3),
        				'tagline' => $this->Model_pengaturan->get_info_id('2',$table3),
        				'tel' => $this->Model_pengaturan->get_info_id('3',$table3),
        				'alamat' => $this->Model_pengaturan->get_info_id('4',$table3),
        				'data' => $this->Model_slider->gettable($table));

		$this->load->view('welcome_message', $data);
		$this->load->view('home/modal_home',$data1);
	}

	public function landing()
	{
		$this->load->helper('url');
		$this->load->view('home/landing_page');
	}
}
