<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Model_pengaturan');
	}

	public function editHalaman(){

		$data = array( 'title' => 'Edit || Pengaturan' );
        if (!$this->session->userdata('akses')) {
            redirect('admin/admin/login');
        }
        $id = $this->uri->segment(4);
        $pages['pages'] = $this->Model_pengaturan->showHalaman_id($id);
        $this->load->view('admin/header',$data);
        $this->load->view('admin/pengaturan/pengaturan_update',$pages);
        $this->load->view('admin/footer');
	}
    public function updateHalaman(){
        $id = $this->input->post('halId');
        $data = array('nama' => $this->input->post('halaman'),
                        'isi_hlm' => $this->input->post('deskripsi'));
        $this->Model_pengaturan->updateHalaman_id($id,$data);
        $this->front();
    }

	public function front(){
        $table = 'options';
        $getInfo = array('nama'=>$this->Model_pengaturan->get_info_id('1',$table),
                        'tag' => $this->Model_pengaturan->get_info_id('2',$table),
                        'tel' =>$this->Model_pengaturan->get_info_id('3',$table),
                        'add' => $this->Model_pengaturan->get_info_id('4',$table),
                        'fb' => $this->Model_pengaturan->get_info_id('5',$table),
                        'tw' => $this->Model_pengaturan->get_info_id('6',$table),
                        'line' => $this->Model_pengaturan->get_info_id('7',$table),
                        'ig' => $this->Model_pengaturan->get_info_id('8',$table),
                        'pages' => $this->Model_pengaturan->get_table() );

        $data = array( 'title' => 'Pengaturan || Halaman Depan' );
        if (!$this->session->userdata('akses')) {
            redirect('admin/admin/login');
        }
        $this->load->view('admin/header',$data);
        $this->load->view('admin/konten/option-front', $getInfo);
        $this->load->view('admin/footer');
    }

    public function updateInfo(){
    	$data = array(
    		array( 'id' => '1',
    				'isi_kolom' => $this->input->post('namaWeb') ),
    		array('id'=>'2',
    				'isi_kolom' => $this->input->post('taglineWeb')),
    		array('id' => '3',
    				'isi_kolom' => $this->input->post('teleponWeb')),
    		array('id' => '4',
    				'isi_kolom' => $this->input->post('alamatWeb')),
    		array('id' => '5',
    			'isi_kolom' => $this->input->post('fbWeb')),
    		array('id' =>'6',
    			'isi_kolom'  => $this->input->post('twWeb')),
    		array('id'=>'7',
    			'isi_kolom' => $this->input->post('lineWeb')),
    		array('id' => '8',
    			'isi_kolom'  => $this->input->post('igWeb')));
    	$this->Model_pengaturan->updateInfo($data,'id');
    	$this->front();
    }
}