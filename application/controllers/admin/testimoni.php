<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
		parent::__construct();
	}

	 public function index(){
        $data = array( 'title' => 'Testimoni' );
        if (!$this->session->userdata('akses')) {
            redirect('admin/admin/login');
        }
        $this->load->view('admin/header',$data);
        $this->load->view('admin/konten/testi_view');
        $this->load->view('admin/footer');
    }

    public function updateTesti(){
    	$data = array( 'title' => 'Testimoni Edit' );
        if (!$this->session->userdata('akses')) {
            redirect('admin/admin/login');
        }
        $this->load->view('admin/header',$data);
        $this->load->view('admin/pengaturan/testi_update');
        $this->load->view('admin/footer');
    }

}