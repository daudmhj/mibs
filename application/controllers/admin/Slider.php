<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
		parent::__construct();
        $this->load->model('Model_slider');
	}

	public function index()
	{   
        $dataHeader = array( 'title' => 'Slider');
        if (!$this->session->userdata('akses')) {
            redirect('admin/admin/login');
        }

        $table = "slider";
        $data['data'] = $this->Model_slider->gettable($table);
        $this->load->view('admin/header',$dataHeader);
        $this->load->view('admin/konten/slider_view', $data);
        $this->load->view('admin/footer');
	}

    
    public function insertSlider()
    {
        if($this->session->userdata('akses'))
        {
            $target_Path = NULL;
            if ($_FILES['path']['name'] != NULL)
            {
                $target_Path = "assets/img/slider/";
                $string = basename( $_FILES['path']['name'] );
                $string = str_replace(" ","-", $string);
                $target_Path = $target_Path.$string;
            }

            if($target_Path!=NULL){
                $data = array(
                    'ID'=> $this->input->post('id'),
                    'Slider'=> $this->input->post('slider'),
                    'Caption'=> $this->input->post('caption'),
                    'Gambar' => $target_Path);
                    
                    $query = $this->Model_slider->insert('slider', $data);
                }
            
            if($query)
            {
                if ($target_Path != NULL) {
                    move_uploaded_file( $_FILES['path']['tmp_name'], $target_Path );
                }
                echo '<script language="javascript">';
                echo 'alert("File berhasil ditambahkan");';
                echo 'window.history.go(-1);';
                echo '</script>';
            }else
            {

                echo '<script language="javascript">';
                echo 'alert("Gagal menyimpan file");';
                echo 'window.history.go(-1);';
                echo '</script>';
            }
            //redirect('admin/tambahartikel/listArtikel');
        }
    }
    public function sliderUpdate(){
        $dataHeader = array( 'title' => 'Slider Edit');
        if (!$this->session->userdata('akses')) {
            redirect('admin/admin/login');
        }
        $id = $this->uri->segment(4);
        $table = "slider";
        $data['data'] = $this->Model_slider->get_data_id($table,$id);
        $this->load->view('admin/header',$dataHeader);
        $this->load->view('admin/pengaturan/slider_update', $data);
        $this->load->view('admin/footer');
    }
}
