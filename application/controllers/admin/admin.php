<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
		parent::__construct();
        $this->load->model('Model_admin');
	}

	public function index()
	{   
        $data = array( 'title' => 'Dashboard' );
		if($this->session->userdata('akses'))
        {
            $this->load->view('admin/header',$data);
            $this->load->view('admin/konten/dashboard');
            $this->load->view('admin/footer');
        }else
        {
            
            $this->load->view('admin/login');
        }
	}

    
    public function login()
    {
        if ($this->session->userdata('akses')) {
            redirect('admin/admin/');
        }

        $this->load->view('admin/login');
        if(isset($_POST['submit']))
        {
            $username   =   $this->input->post('username');
            $password   =   $this->input->post('password');
            $hasil      =   $this->Model_admin->login($username,$password);
            if($hasil==TRUE)
            {
                $this->session->set_userdata('akses',TRUE);
                redirect ('admin/admin');
            } else{
                echo '<script language="javascript">';
                echo 'alert("Login Gagal. Perhatikan username password");';
                echo 'window.history.go(-1);';
                echo '</script>';
                $this->load->view('admin/login');
            }
        }
    }


    public function message(){
        $data = array( 'title' => 'Pesan Masuk' );
        if (!$this->session->userdata('akses')) {
            redirect('admin/admin/login');
        }
        $this->load->view('admin/header',$data);
        $this->load->view('admin/konten/message_view');
        $this->load->view('admin/footer');
    }

    public function userProfile(){

        $data = array( 'title' => 'User Profile');
        if (!$this->session->userdata('akses')) {
            redirect('admin/admin/login');
        }
        $this->load->view('admin/header',$data);
        $this->load->view('admin/pengaturan/userProfile_view');
        $this->load->view('admin/footer');
    }
    
    public function logout()
    {
            $this->session->sess_destroy();
            redirect('welcome');
    }
}
