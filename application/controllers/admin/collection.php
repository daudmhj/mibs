<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collection extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Model_book');
    }


public function index(){
        $data = array( 'title' => 'Koleksi Buku' );
        if (!$this->session->userdata('akses')) {
            redirect('admin/admin/login');
        }
        $table1 = 'penerbit';
        $data1 = array('shPenerbit' => $this->Model_book->showSelection($table1));

        $this->load->view('admin/header',$data);
        $this->load->view('admin/konten/collection_view',$data1);
        $this->load->view('admin/footer');
    }

    public function penerbit(){
        $data = array( 'title' => 'Penerbit & Kategori');
        if (!$this->session->userdata('akses')) {
            redirect('admin/admin/login');
        }

        $table1= 'penerbit';
        $table2 = 'categories';
        $data1 = array('penerbit' => $this->Model_book->getFromTable($table1),
                        'categories' => $this->Model_book->getFromTable($table2));

        $this->load->view('admin/header',$data);
        $this->load->view('admin/konten/penerbit_view', $data1);
        $this->load->view('admin/footer');   
    }

    public function bookUpdate(){
    	$data = array( 'title' => 'Buku Edit');
        if (!$this->session->userdata('akses')) {
            redirect('admin/admin/login');
        }
        $this->load->view('admin/header',$data);
        $this->load->view('admin/pengaturan/book_update');
        $this->load->view('admin/footer');	
    }
    public function addPenerbit(){
        // validasi
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama','Nama Penerbit','required|is_unique[penerbit.nama]');
        $this->form_validation->set_error_delimiters('<div style="color:red">', '</div>');

        if ($this->form_validation->run() == FALSE) {
        $this->penerbit();
        } else {

        $data1 = array(
                'id' => '',
                'nama' => $this->input->post('nama'));

        $this->Model_book->insert('penerbit',$data1);

        $data['msg_pen'] = 'Berhasil ditambahkan';
        $this->penerbit();
        }
    }

}    