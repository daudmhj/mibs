<?php
class Model_pengaturan extends CI_Model
{
    public function __construct() {
        parent::__construct();
        
    }

    public function get_info_id($id,$table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id',$id);
        $query = $this->db->get();

        if ( $query->num_rows() > 0 )
        {
            $row = $query->row_array();
            return $row;
        }
    }

    public function showHalaman_id($data){
        $this->db->select('*');
        $this->db->from('pages');
        $this->db->where('id', $data);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    public function updateHalaman_id($id,$data){
        $this->db->where('id',$id);
        $this->db->update('pages',$data);
    }

    public function updateInfo($data,$where){
        $this->db->update_batch('options', $data, $where);
    }

    public function get_table(){
        $this->db->select('*');
        $this->db->from('pages');
        $query = $this->db->get();
        return $result = $query->result_array();
    }
}
