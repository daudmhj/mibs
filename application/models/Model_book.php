<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_book extends CI_Model {
	public function __construct() {
		parent::__construct();
		
	}

	public function getFromTable($tablename){
		return $this->db->query("SELECT * FROM $tablename ORDER BY id ASC")->result_array();
	}

	public function insert($tablename, $where){
		return $this->db->insert($tablename, $where);
	}
	public function showSelection($tablename){
		$query = $this->db->get($tablename);
		$query_result = $query->result();
		return $query_result;
	}
}	